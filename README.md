# Sample play-scala/Activator application

A sample application to demonstrate play-scala/Activator continuous integration in Gitlab.

## Code
The project content is based on the Activator `play-scala` template.

## Prerequisites
You will need to have **Activator** installed to run the project.


## Run tests
Execute `activator test` in the project root directory.
